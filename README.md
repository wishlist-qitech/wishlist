# Wishlist

API para gerir uma lista de desejos.

# **Instruções de execução para o Docker.**

Para executar os comandos abaixo é necessário que o Docker esteja instalado na maquina.

Abra um terminal dentro da pasta que contem o arquivo docker-compose.yml e execute o comando.

`docker-compose up --build`

Será iniciado o processo de instalação e configuração da aplicação no Docker.

Ao finalizar abra o Docker e inicie os dois containeres do servidor.

# Executando a API

Todos os endpoints da API estão documentados no próprio servidor integrado a OpenAPI 3.0.

A documentação está disponível em `localhost:8000\docs`

Para testar os endpoints e realizar o cadastro de um item na lista de desejos é necessário registrar um usuario 
e senha no servidor e realizar o login com o Token gerado.

Todos os testes dos endpoints podem ser feitos na própria OpenAPI 3.0

# Instalação manual

Caso seja necessária uma instalação da instacia da API em ambiente local de forma manual é necessário executar os 
seguintes requisistos:

**Python**

Instalar o Python na versão 3.9 junto das bibliotecas adicionais listadas no arquivo requirements.txt.

**Postgree SQL**

Criar um banco de dados no servidor com o nome de wishlist e configurar a string de conexão em database.py apontando 
para esse banco de dados.

**Servidor**

Iniciar uma instancia do uvicorn no terminal com o comando `uvicorn wishlist.main:app` dentro da pasta que contem o arquivo docker-compose.yml.





