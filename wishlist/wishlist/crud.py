from sqlalchemy.orm import Session
from sqlalchemy.sql import text
from sqlalchemy import update
from sqlalchemy import delete

from . import models, schemas
from .auth import AuthHandler
import uuid



def get_wishlist_by_id(db: Session, wish_id: str):
    return db.query(models.Wishlist).filter(models.Wishlist.id == wish_id).first()

def busca_itens_adquiridos(db: Session, usuario: str):
    return db.query(models.Wishlist).filter(models.Wishlist.adquirido == "s", models.Wishlist.usuario == usuario).all()

def get_wishlist(db: Session, usuario: str):
    return db.query(models.Wishlist).filter(models.Wishlist.usuario == usuario).all()

def create_wish(db: Session, wish: schemas.WishCreate, usuario:str):
    chave_id = str(uuid.uuid1())
    db_wishlist = models.Wishlist(
        id=chave_id, 
        titulo=wish.titulo,
        descricao=wish.descricao,
        link = wish.link,
        foto = wish.foto,
        usuario = usuario
    )
    db.add(db_wishlist)
    db.commit()
    db.refresh(db_wishlist)
    return db_wishlist

def atualiza_situacao_wishlist(db: Session, wish: schemas.WishUpdateSituacao):
    chave = wish.id
    #Verifica se existe um desejo com o id informado
    wishlist = get_wishlist_by_id(db, chave)
    if wishlist:
        db.query(models.Wishlist).filter(models.Wishlist.id == chave).\
        update({"adquirido": "s"}, synchronize_session="fetch")   
        db.commit()
        return {"Mensagem": "Status do item atualizado como ADQUIRIDO"}
    return {"Mensagem": "Chave não encontrada!"}

def atualiza_wishlist(db: Session, wish: schemas.WishUpdate):
    chave = wish.id
    #Verifica se existe um desejo com o id informado
    wishlist = get_wishlist_by_id(db, chave)

    if wishlist:
        stmt = update(models.Wishlist).where(models.Wishlist.id == chave).values(
            titulo=wish.titulo,
            descricao=wish.descricao,
            link = wish.link,
            foto = wish.foto
            ).\
        execution_options(synchronize_session="fetch")

        db.execute(stmt) 
        db.commit()

        return get_wishlist_by_id(db, chave)
    return {"Mensagem":"Chave não encontrada!"}

def retornar_id_aleatorio(db: Session):
    string_sql = text("SELECT id FROM wishlist OFFSET floor(random() * (SELECT COUNT(*) FROM wishlist))LIMIT 1")
    resultado = db.execute(string_sql).fetchall() # retorna uma lista de tuplas 
    lista_id = []
    for tupla in resultado:
        lista_id.append(tupla[0])
    return get_wishlist_by_id(db, lista_id[0])

def deleta_wishlist(db: Session, wish: schemas.WishUpdateSituacao):
    chave = wish.id
    #Verifica se existe um desejo com o id informado
    wishlist = get_wishlist_by_id(db, chave)
    
    if wishlist:
        db.query(models.Wishlist).filter(models.Wishlist.id == chave).\
        delete(synchronize_session="fetch")  
        db.commit()
        return {"Mensagem": "Item deletado com sucesso"}
    return {"Mensagem": "Chave não encontrada!"}

## Sessão de autenticação
def criar_usuario(db: Session, usuario: schemas.UsuarioBase):
    
    auth_handler = AuthHandler()
    
    chave_id = str(uuid.uuid1())
    db_wishlist = models.Usuario(
        id=chave_id, 
        usuario = usuario.usuario,
        senha = usuario.senha
    )
    db.add(db_wishlist)
    db.commit()
    db.refresh(db_wishlist)
    return db_wishlist

def busca_usuario_por_username(db: Session, usuario: str):
    if db.query(models.Usuario).filter(models.Usuario.usuario == usuario).first() == None:
        return True
    else:
        return False

def busca_usuario(db: Session, usuario: str, senha: str):
    
    auth_handler = AuthHandler()

    if db.query(models.Usuario).filter(models.Usuario.usuario == usuario, models.Usuario.senha == senha).first() == None:
        return False
    else:
        return True
    



