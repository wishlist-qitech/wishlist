from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

#Configuração para o docker
SQLALCHEMY_DATABASE_URL = "postgresql://postgres:12345@db:5432/wishlist"

#Configuração para desenvolvimento local
#SQLALCHEMY_DATABASE_URL = "postgresql://postgres:12345@127.0.0.1:5432/wishlist"

engine = create_engine(SQLALCHEMY_DATABASE_URL)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind= engine)
Base = declarative_base()