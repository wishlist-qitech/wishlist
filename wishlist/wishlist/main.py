from typing import List
from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session

from . import crud,models,schemas
from .auth import AuthHandler
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

auth_handler = AuthHandler()

## Lista de Requisições
@app.post("/registrar/", status_code=201)
def registrar(usuario_schema: schemas.UsuarioBase, db: Session = Depends(get_db)):
    if crud.busca_usuario_por_username(db=db,usuario=usuario_schema.usuario):
        return crud.criar_usuario(db=db, usuario=usuario_schema)
    else:
        return {"Mensagem":"Usuário indisponivel!"}

@app.post("/login/")
def logar(usuario_schema : schemas.UsuarioBase, db: Session = Depends(get_db)):
    if(crud.busca_usuario(db=db,usuario=usuario_schema.usuario,senha=usuario_schema.senha)):
        token = auth_handler.encode_token(usuario_schema.usuario)
        return { 'token': token }
    raise HTTPException(status_code=401, detail='Usuário e senha invalidos!'+ usuario_schema.senha)

@app.post("/wishlist/", response_model=schemas.Wish)
def criar_desejo(wish: schemas.WishCreate, db: Session = Depends(get_db), usuario=Depends(auth_handler.auth_wrapper)):
    return crud.create_wish(db=db, wish=wish, usuario=usuario)

@app.get("/wishlist/", response_model=List[schemas.Wish])
def get_desejos(usuario=Depends(auth_handler.auth_wrapper), db: Session = Depends(get_db)):
    return crud.get_wishlist(db,usuario=usuario)

@app.get("/wishlist/random/", response_model=schemas.Wish)
def get_desejo_aleatorio(usuario=Depends(auth_handler.auth_wrapper), db: Session = Depends(get_db)):
    return crud.retornar_id_aleatorio(db)

@app.get("/wishlist/adquiridos/", response_model=List[schemas.Wish])
def get_itens_adquiridos(usuario=Depends(auth_handler.auth_wrapper), db: Session = Depends(get_db)):
    db_wishlist = crud.busca_itens_adquiridos(db, usuario)
    if db_wishlist is None:
        raise HTTPException(status_code=404, detail="Não há itens adquiridos!")
    return db_wishlist

@app.get("/wishlist/{wish_id}/", response_model=schemas.Wish)
def get_desejo_id(wish_id: str, db: Session = Depends(get_db),usuario=Depends(auth_handler.auth_wrapper)):
    db_wishlist = crud.get_wishlist_by_id(db, wish_id=wish_id)
    if db_wishlist is None:
        raise HTTPException(status_code=404, detail="Item não está na lista de desejos!")
    return db_wishlist

@app.put("/wishlist/adquiridos/")
def put_situacao_desejo(wish: schemas.WishUpdateSituacao, db: Session = Depends(get_db), usuario=Depends(auth_handler.auth_wrapper)):
    return crud.atualiza_situacao_wishlist(db=db, wish=wish)

@app.put("/wishlist/")
def atualiza_item_desejo(wish: schemas.WishUpdate, db: Session = Depends(get_db), usuario=Depends(auth_handler.auth_wrapper)):
    return crud.atualiza_wishlist(db=db, wish=wish)

@app.delete("/wishlist/")
def deleta_item_desejo(wish : schemas.WishUpdateSituacao, db: Session = Depends(get_db), usuario=Depends(auth_handler.auth_wrapper)):
    return crud.deleta_wishlist(db=db, wish=wish)




    