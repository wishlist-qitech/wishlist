from sqlalchemy import Column, String
from sqlalchemy.sql.sqltypes import CHAR

from .database import Base

class Wishlist(Base):
    __tablename__ = "wishlist"

    id = Column(String, primary_key=True, index=True)
    titulo = Column(String)
    descricao = Column(String)
    link = Column(String)
    foto = Column(String)
    adquirido = Column(CHAR)
    usuario = Column(String)

class Usuario(Base):

    __tablename__ = "usuarios"

    id = Column(String, primary_key=True, index=True)
    usuario = Column(String)
    senha = Column(String)