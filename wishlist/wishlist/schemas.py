from typing import List, Optional
from pydantic import BaseModel


class WishBase(BaseModel):
    id: str
    titulo: str
    descricao : Optional[str]
    link : Optional[str]
    foto : Optional[str]
    adquirido: Optional[str]

class WishCreate(BaseModel):
    titulo : str
    descricao : Optional[str]
    link : Optional[str]
    foto : Optional[str]
    adquirido: Optional[str]

class WishUpdate(BaseModel):
    id: str
    titulo : str
    descricao : Optional[str]
    link : Optional[str]
    foto : Optional[str]

class WishUpdateSituacao(BaseModel):
    id: str

class WishUpdateRetorno(BaseModel):
    mensagem : str

class Wish(BaseModel):
    id: str
    titulo: str
    descricao : Optional[str]
    link : Optional[str]
    foto : Optional[str]
    adquirido: Optional[str]
    
    class Config:
        orm_mode = True


class UsuarioBase(BaseModel):
    usuario: str
    senha: str